import logic.Operation;
import model.Monomial;
import model.Polynomial;
import org.junit.jupiter.api.Test;
import utils.PolynomialRegex;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OperationTest {

    private final PolynomialRegex polynomialRegex = new PolynomialRegex();
    private final String firstPolynomialString="x^3-2x^2+6x-5";
    private final String secondPolynomialString="x^2-1";
    private final Polynomial firstPolynomial = polynomialRegex.transformStringIntoPolynomial(firstPolynomialString);
    private final Polynomial secondPolynomial = polynomialRegex.transformStringIntoPolynomial(secondPolynomialString);

    @Test
    public void addTest(){
        Operation op=new Operation();
        String resultString = "x^3-x^2+6x-6";
        Polynomial result = new Polynomial();
        result.getMonomials().add(new Monomial(1,3));
        result.getMonomials().add(new Monomial(-1,2));
        result.getMonomials().add(new Monomial(6,1));
        result.getMonomials().add(new Monomial(-6,0));
        assertTrue(op.addition(firstPolynomial,secondPolynomial).equals(result), "Addition works");
        assertEquals(polynomialRegex.polynomialToString(result), resultString, "Addition works");
    }

    @Test
    public void subtractTest(){
        Operation op=new Operation();
        String resultString = "x^3-3x^2+6x-4";
        Polynomial result = new Polynomial();
        result.getMonomials().add(new Monomial(1,3));
        result.getMonomials().add(new Monomial(-3,2));
        result.getMonomials().add(new Monomial(6,1));
        result.getMonomials().add(new Monomial(-4,0));
        assertTrue(op.subtraction(firstPolynomial,secondPolynomial).equals(result), "Subtraction works");
        assertEquals(polynomialRegex.polynomialToString(result), resultString, "Subtraction works");
    }

    @Test
    public void multiplyTest(){
        Operation op=new Operation();
        String resultString = "x^5-2x^4+5x^3-3x^2-6x+5";
        Polynomial result = new Polynomial();
        result.getMonomials().add(new Monomial(1,5));
        result.getMonomials().add(new Monomial(-2,4));
        result.getMonomials().add(new Monomial(5,3));
        result.getMonomials().add(new Monomial(-3,2));
        result.getMonomials().add(new Monomial(-6,1));
        result.getMonomials().add(new Monomial(5,0));
        assertTrue(op.multiplication(firstPolynomial,secondPolynomial).equals(result), "Multiplication works");
        assertEquals(polynomialRegex.polynomialToString(result), resultString, "Multiplication works");
    }

    @Test
    public void divideTest(){
        Operation op=new Operation();
        String quotientString = "x-2";
        String remainderString= "7x-7";
        Polynomial quotient = new Polynomial();
        Polynomial remainder = new Polynomial();
        quotient.getMonomials().add(new Monomial(1,1));
        quotient.getMonomials().add(new Monomial(-2,0));
        remainder.getMonomials().add(new Monomial(7,1));
        remainder.getMonomials().add(new Monomial(-7,0));

        Polynomial[] resultObtained = op.division(firstPolynomial,secondPolynomial);
        assertTrue(resultObtained[0].equals(quotient), "Correct quotient");
        assertTrue(resultObtained[1].equals(remainder), "Correct remainder");
        assertEquals(polynomialRegex.polynomialToString(resultObtained[0]), quotientString, "Correct quotient");
        assertEquals(polynomialRegex.polynomialToString(resultObtained[1]), remainderString, "Correct remainder");
    }

    @Test
    public void differentiateTest(){
        Operation op = new Operation();
        String resultString = "3x^2-4x+6";
        Polynomial result = new Polynomial();
        result.getMonomials().add(new Monomial(3,2));
        result.getMonomials().add(new Monomial(-4,1));
        result.getMonomials().add(new Monomial(6,0));
        assertTrue(op.derivative(firstPolynomial).equals(result), "Differentiation works");
        assertEquals(polynomialRegex.polynomialToString(result), resultString, "Differentiation works");
    }

    @Test
    public void integrationTest(){
        Operation op = new Operation();
        String resultString = "0.3333333333333333x^3-x+C";
        Polynomial result = new Polynomial();
        result.getMonomials().add(new Monomial(0.3333333333333333,3));
        result.getMonomials().add(new Monomial(-1,1));
        assertTrue(op.integration(secondPolynomial).equals(result), "Integration works");
        String resultObtainedString = polynomialRegex.polynomialToString(result) + "+C";
        assertEquals(resultObtainedString, resultString, "Integration works");
    }
}
