package gui;

import logic.Operation;
import model.Polynomial;
import utils.PolynomialRegex;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller implements ActionListener {

    private View view;

    public Controller(View v){
        this.view = v;
    }

    private Polynomial[] validateInput(String firstPolynomialString,String secondPolynomialString,String operation){

        PolynomialRegex polynomialRegex = new PolynomialRegex();
        if(!polynomialRegex.validatePolynomial(firstPolynomialString)){
            return null;
        }else if(!polynomialRegex.validatePolynomial(secondPolynomialString) && !operation.equals("Differentiation") && !operation.equals("Integration")){
            return null;
        }

        Polynomial firstPolynomial = polynomialRegex.transformStringIntoPolynomial(firstPolynomialString);
        Polynomial secondPolynomial = polynomialRegex.transformStringIntoPolynomial(secondPolynomialString);

        if(firstPolynomial==null || secondPolynomial==null){
            return null;
        }
        Polynomial[] input = new Polynomial[2];
        input[0]=firstPolynomial;
        input[1]=secondPolynomial;
        return input;
    }

    private void generateResult(Polynomial result,Polynomial[] resultDivision,String operation){
        PolynomialRegex polynomialRegex=new PolynomialRegex();
        if(!operation.equals("Division")) {
            String resultString = polynomialRegex.polynomialToString(result);
            if(operation.equals("Integration")){
                resultString=resultString+"+C";
            }
            view.getResultValueLabel().setText(resultString);
        }else{
            if(resultDivision==null){
                JOptionPane.showMessageDialog(null, "ERROR: Division by 0 !","ERROR",JOptionPane.ERROR_MESSAGE);
            }else{
                String resultStringQuotient = polynomialRegex.polynomialToString(resultDivision[0]);
                String resultStringReminder = polynomialRegex.polynomialToString(resultDivision[1]);
                view.getResultValueLabel().setText("Quotient: " + resultStringQuotient + "     \nReminder: " + resultStringReminder);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if(command.equals("COMPUTE") ){
            String firstPolynomialString = view.getFirstPolynomialTextField().getText();
            String secondPolynomialString = view.getSecondPolynomialTextField().getText();
            Operation operations = new Operation();
            String operation = String.valueOf(view.getOperationsComboBox().getSelectedItem());

            Polynomial[] input = validateInput(firstPolynomialString,secondPolynomialString,operation);
            if(input==null){
                JOptionPane.showMessageDialog(null, "ERROR: Wrong input !","ERROR",JOptionPane.ERROR_MESSAGE);
                return;
            }

            Polynomial result = new Polynomial();
            Polynomial[] resultDivision = new Polynomial[2];
            switch(operation){
                case "Addition": result = operations.addition(input[0],input[1]);
                    break;
                case "Subtraction": result = operations.subtraction(input[0],input[1]);
                    break;
                case "Multiplication": result = operations.multiplication(input[0],input[1]);
                    break;
                case "Division": resultDivision = operations.division(input[0],input[1]);
                    break;
                case "Differentiation": result = operations.derivative(input[0]);
                    break;
                case "Integration": result = operations.integration(input[0]);
                    break;
            }
            generateResult(result,resultDivision,operation);
        }
    }
}
