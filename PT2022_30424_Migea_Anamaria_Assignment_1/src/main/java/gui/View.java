package gui;

import javax.swing.*;
import java.awt.*;

public class View extends JFrame{
    private JPanel contentPane;
    private JPanel polynomialsPanel;
    private JLabel firstPolynomialLabel;
    private PTextField firstPolynomialTextField;
    private JLabel secondPolynomialLabel;
    private PTextField secondPolynomialTextField;
    private JLabel operationsLabel;
    private JComboBox operationsComboBox;
    private JButton computeButton;
    private JPanel resultPanel;
    private JLabel resultLabel;
    private JLabel resultValueLabel;
    private JPanel infoPanel;
    private JLabel welcomeLabel;
    private JLabel infoLabel;

    protected Controller controller = new Controller(this);

    public View(String name) {
        super(name);
        this.prepareGui();
    }

    public void prepareGui(){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height = screenSize.height;
        int width = screenSize.width;
        this.setSize(width/2, height/2);
        // here's the part where I center the frame on screen
        this.setLocationRelativeTo(null);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.contentPane = new JPanel(new GridLayout(3, 1));
        this.prepareInfoPanel();
        this.preparePolynomialsPanel();
        this.prepareResultPanel();
        this.setContentPane(this.contentPane);

    }

    private void prepareInfoPanel(){
        this.infoPanel = new JPanel();
        this.infoPanel.setBackground(Color.PINK);
        this.infoPanel.setLayout(new GridLayout(2,1));
        this.welcomeLabel = new JLabel("Hi, user!",JLabel.CENTER);
        this.welcomeLabel.setFont(new Font("Times New Roman", Font.ITALIC, 30));
        this.infoLabel = new JLabel("->differentiation and integration are computed for te first polynomial",JLabel.CENTER);
        this.infoLabel.setFont(new Font("Times New Roman", Font.ITALIC, 20));
        this.infoPanel.add(this.welcomeLabel);
        this.infoPanel.add(this.infoLabel);
        this.contentPane.add(this.infoPanel);
    }

    private void prepareResultPanel() {
        this.resultPanel = new JPanel();
        this.resultPanel.setBackground(Color.CYAN);
        this.resultPanel.setLayout(new GridLayout(2,2));
        this.resultLabel = new JLabel("Result", JLabel.CENTER);
        this.resultValueLabel = new JLabel("", JLabel.CENTER);
        this.resultValueLabel.setFont(new Font("Times New Roman", Font.PLAIN, 30));
        this.resultPanel.add(this.resultLabel);
        this.resultPanel.add(this.resultValueLabel);
        this.contentPane.add(this.resultPanel);
    }

    private void preparePolynomialsPanel() {
        this.polynomialsPanel = new JPanel();
        this.polynomialsPanel.setBackground(Color.CYAN);
        this.polynomialsPanel.setLayout(new GridLayout(4, 2));

        this.firstPolynomialLabel = new JLabel("First polynomial", JLabel.CENTER);
        this.polynomialsPanel.add(this.firstPolynomialLabel);
        this.firstPolynomialTextField = new PTextField("like 'x^2+3x+2'");
        this.polynomialsPanel.add(this.firstPolynomialTextField);

        this.secondPolynomialLabel = new JLabel("Second polynomial", JLabel.CENTER);
        this.polynomialsPanel.add(secondPolynomialLabel);
        this.secondPolynomialTextField = new PTextField("like '2x^7+10'");
        this.polynomialsPanel.add(secondPolynomialTextField);

        this.operationsLabel = new JLabel("Select operation", JLabel.CENTER);
        this.polynomialsPanel.add(this.operationsLabel);

        String[] operations = new String[]{"Addition", "Subtraction", "Multiplication", "Division", "Differentiation", "Integration"};
        this.operationsComboBox = new JComboBox(operations);
        this.polynomialsPanel.add(operationsComboBox);

        this.computeButton = new JButton("Compute");
        this.computeButton.setActionCommand("COMPUTE");
        this.computeButton.addActionListener(this.controller);
        this.polynomialsPanel.add(this.computeButton);

        this.contentPane.add(this.polynomialsPanel);

    }

    public PTextField getFirstPolynomialTextField() {
        return firstPolynomialTextField;
    }

    public PTextField getSecondPolynomialTextField() {
        return secondPolynomialTextField;
    }

    public JComboBox getOperationsComboBox() {
        return operationsComboBox;
    }

    public JLabel getResultValueLabel() {
        return resultValueLabel;
    }

}
