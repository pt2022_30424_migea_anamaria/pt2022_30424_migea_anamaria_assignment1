package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class PTextField extends JTextField {

    public PTextField(final String promptText) {
        super(promptText);
        setText(promptText);
        setForeground(Color.GRAY);
        setFont(new Font("Times New Roman", Font.ITALIC, 20));
        addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if(getText().equals(promptText)) {
                    setText("");
                    setForeground(Color.BLACK);
                    setFont(new Font("Times New Roman", Font.PLAIN, 20));
                }
            }
            @Override
            public void focusLost(FocusEvent e) {
                if(getText().isEmpty()) {
                    setText(promptText);
                    setForeground(Color.GRAY);
                    setFont(new Font("Times New Roman", Font.ITALIC, 20));
                }
            }

        });
    }

}