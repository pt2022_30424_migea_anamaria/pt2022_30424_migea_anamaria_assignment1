package utils;

import model.Monomial;
import model.Polynomial;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PolynomialRegex {
    private static final String POLYNOMIAL_PATTERN = "((?<!\\+|-)(((-|[\\+])?[0-9]*)((?<!x)x([\\^][0-9]+)?)?)(?<!\\+|-))+";
    private static final String MONOMIAL_PATTERN = "(?<!\\+|-)(((-|[\\+])?[0-9]*)((?<!x)x([\\^][0-9]+)?)?)(?<!\\+|-)";

    public boolean validatePolynomial(String polynomialString){
        Pattern patternPolynomial = Pattern.compile(POLYNOMIAL_PATTERN);
        Matcher matcherPolynomial = patternPolynomial.matcher(polynomialString);

        return matcherPolynomial.matches() && !polynomialString.equals("");
    }

    public Polynomial transformStringIntoPolynomial(String polynomialString){
        Pattern patternMonomial = Pattern.compile(MONOMIAL_PATTERN);
        Matcher matcherMonomial = patternMonomial.matcher(polynomialString);

        Polynomial polynomial = new Polynomial();
        boolean firstMonomial = true;

        while(matcherMonomial.find() && !matcherMonomial.group(1).equals("")) { //the second condition is bc the empty string also matches monomial pattern
            String coefficient = matcherMonomial.group(2);
            String signCoefficient = matcherMonomial.group(3);
            String xPower = matcherMonomial.group(4);
            String power = matcherMonomial.group(5);
            Monomial monomial = transformStringIntoMonomial(coefficient,signCoefficient,xPower,power,firstMonomial);
            if(monomial==null){  //this is if I introduce something like x^2x+...
                return null;
            }
            polynomial.addMonomial(monomial);
            firstMonomial=false;

//            System.out.println("found: <" + matcherMonomial.group(1) +
//                    "> <" + matcherMonomial.group(2) +
//                    "> <" + matcherMonomial.group(3) +
//                    "> <" + matcherMonomial.group(4) +
//                    "> <" + matcherMonomial.group(5) + ">");
        }
        return polynomial;
    }

    private Monomial transformStringIntoMonomial(String coefficient, String signCoefficient, String xPower, String power, boolean firstMonomial){
        int monomialCoefficient;
        int monomialPower;
        if(signCoefficient==null){
            if(!firstMonomial){
                return null;
            }else{
                if(coefficient.equals("") || coefficient.equals("+")){
                    monomialCoefficient = 1;
                }else if(coefficient.equals("-")){
                    monomialCoefficient = -1;
                }else{
                    monomialCoefficient = Integer.parseInt(coefficient);
                }
            }
        }else{
            if(signCoefficient.equals(coefficient)){
                if(signCoefficient.equals("+")){
                    monomialCoefficient = 1;
                }else{
                    monomialCoefficient = -1;
                }
            }else{
                if(signCoefficient.equals("+")){
                    monomialCoefficient = Integer.parseInt(coefficient.substring(1));
                }else{
                    monomialCoefficient = Integer.parseInt(coefficient);
                }
            }
        }
        if(xPower==null){
            monomialPower=0;
        }else{
            if(power==null){
                monomialPower=1;
            }else{
                monomialPower=Integer.parseInt(power.substring(1));
            }
        }
        return new Monomial(monomialCoefficient,monomialPower);
    }

    public String polynomialToString(Polynomial polynomial){
        String polynomialString="";
        boolean firstMonomial = true;
        if(polynomial.getMonomials().size()==0){
            polynomialString="0";
        }else{
            for (Monomial monomial: polynomial.getMonomials()){
                polynomialString = polynomialString + monomialToString(monomial,firstMonomial);
                firstMonomial=false;
            }
        }
        return polynomialString;
    }
    private boolean checkIntCoefficient(double coefficient){
        return coefficient == (int) coefficient;
    }

    private String doubleOrIntCoefficientToString(double coefficient) {
        if(checkIntCoefficient(coefficient)) {
            return String.valueOf((int)coefficient);
        }
        return String.valueOf( coefficient);
    }

    private String monomialToString(Monomial monomial,boolean firstMonomial){
        String monomialString;
        double coefficient = monomial.getCoefficient();
        int power = monomial.getPower();
        String powerString="x^";
        if(power==0){
            powerString="";
        }else if(power>0){
            if(power==1){
                powerString="x";
            }else{
                powerString=powerString+power;
            }
        }
        String coefficientString="";
        if(coefficient<0){
            if(coefficient==-1){
                if(powerString.equals("")){
                    coefficientString=doubleOrIntCoefficientToString(coefficient);
                }else{
                    coefficientString="-";
                }
            }else{
                coefficientString=doubleOrIntCoefficientToString(coefficient);
            }
        }else if(coefficient==0){
            coefficientString="";
            powerString="";
        }else{
            if(!firstMonomial){
                if(coefficient>0) {
                    coefficientString = "+";
                }
            }
            if(coefficient==1){
                if(powerString.equals("")){
                    coefficientString=coefficientString+doubleOrIntCoefficientToString(coefficient);
                }
            }else{
                coefficientString=coefficientString+doubleOrIntCoefficientToString(coefficient);
            }
        }
        monomialString=coefficientString+powerString;
        return monomialString;
    }
}
