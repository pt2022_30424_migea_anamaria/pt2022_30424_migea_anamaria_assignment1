package logic;

import model.Monomial;
import model.Polynomial;

public class Operation {

    //poly1+poly2
    public Polynomial addition(Polynomial poly1,Polynomial poly2){
        poly1.sortMonomialsByDegree();
        poly2.sortMonomialsByDegree();
        Polynomial result=poly1.copyPolynomial();
        for (Monomial monomialPoly2 : poly2.getMonomials()) {
            int powerPoly2 = monomialPoly2.getPower();
            double coefficientPoly2 = monomialPoly2.getCoefficient();
            Monomial searchPowerMonomial = result.searchPower(powerPoly2);
            if (searchPowerMonomial == null) {
                result.getMonomials().add(monomialPoly2);
            } else {
                searchPowerMonomial.setCoefficient(coefficientPoly2 + searchPowerMonomial.getCoefficient());
            }
        }
        result.sortMonomialsByDegree();
        result.deleteZeroTerms();
        return result;
    }

    //poly1-poly2
    public Polynomial subtraction(Polynomial poly1,Polynomial poly2){
        poly1.sortMonomialsByDegree();
        poly2.sortMonomialsByDegree();
        Polynomial result = poly1.copyPolynomial();
        Polynomial poly2Copy = poly2.copyPolynomial();
        for (Monomial monomialPoly2 : poly2Copy.getMonomials()) {
            int powerPoly2 = monomialPoly2.getPower();
            double coefficientPoly2 = monomialPoly2.getCoefficient();
            Monomial searchPowerMonomial = result.searchPower(powerPoly2);
            if (searchPowerMonomial == null) {
                result.getMonomials().add(monomialPoly2);
                monomialPoly2.setCoefficient((-1) * coefficientPoly2);
            } else {
                searchPowerMonomial.setCoefficient(searchPowerMonomial.getCoefficient() - coefficientPoly2);
            }
        }
        result.sortMonomialsByDegree();
        result.deleteZeroTerms();
        return result;
    }

    //poly1*poly2
    public Polynomial multiplication(Polynomial poly1,Polynomial poly2){
        poly1.sortMonomialsByDegree();
        poly2.sortMonomialsByDegree();
        Polynomial result=new Polynomial();
        for (Monomial monomialPoly1 : poly1.getMonomials()) {
            int powerPoly1 = monomialPoly1.getPower();
            double coefficientPoly1 = monomialPoly1.getCoefficient();
            for(Monomial monomialPoly2 : poly2.getMonomials()){
                int powerPoly2 = monomialPoly2.getPower();
                double coefficientPoly2 = monomialPoly2.getCoefficient();
                int powerRes = powerPoly1 + powerPoly2;
                double coefficientRes = coefficientPoly1 * coefficientPoly2;
                Monomial searchPowerMonomial = result.searchPower(powerRes);
                if(searchPowerMonomial==null){
                    result.getMonomials().add(new Monomial(coefficientRes,powerRes));
                }else{
                    searchPowerMonomial.setCoefficient(searchPowerMonomial.getCoefficient() + coefficientRes);
                }
            }
        }
        result.sortMonomialsByDegree();
        result.deleteZeroTerms();
        return result;
    }

    //poly1/poly2
    public Polynomial[] division(Polynomial poly1,Polynomial poly2){
        poly1.sortMonomialsByDegree();
        poly2.sortMonomialsByDegree();
        poly2.deleteZeroTerms();
        if(poly2.getMonomials().size()==0 || (poly2.getMonomials().size()==1 && poly2.getMonomials().get(0).getPower()==0 && poly2.getMonomials().get(0).getCoefficient()==0)){
            return null; //if division by 0, return null as result
        }
        Polynomial quotient = new Polynomial();
        Polynomial remainder;
        if(poly1.degree()< poly2.degree()){
            quotient.getMonomials().add(new Monomial(0,0));
            remainder = poly1.copyPolynomial();
        }else{
            Polynomial q = poly1.copyPolynomial();
            Polynomial r = poly2.copyPolynomial();
            while(q.getMonomials().size()!=0 && q.getMonomials().get(0).getPower()>=poly2.getMonomials().get(0).getPower()){
                int diffPower = q.getMonomials().get(0).getPower() - poly2.getMonomials().get(0).getPower();
                double mul_c = q.getMonomials().get(0).getCoefficient() / poly2.getMonomials().get(0).getCoefficient();
                quotient.getMonomials().add(new Monomial(mul_c,diffPower));
                Polynomial poly = r.copyPolynomial();
                for(Monomial monomial: poly.getMonomials()){
                    int power = monomial.getPower();
                    double coefficient = monomial.getCoefficient();
                    monomial.setCoefficient(coefficient*mul_c);
                    monomial.setPower(power+diffPower);
                }
                q=this.subtraction(q,poly);
                q.getMonomials().removeIf(monomial -> monomial.getCoefficient() == 0);
            }
            remainder=q;
        }
        Polynomial[] result = new Polynomial[2];
        quotient.deleteZeroTerms();
        remainder.deleteZeroTerms();
        result[0]=quotient;
        result[1]=remainder;
        return result;
    }

    //(poly)'
    public Polynomial derivative(Polynomial poly){
        Polynomial result = poly.copyPolynomial();
        for(Monomial monomial: result.getMonomials()){
            int power=monomial.getPower();
            double coefficient=monomial.getCoefficient();
            monomial.setCoefficient(coefficient*power);
            monomial.setPower(power-1);
        }
        result.deleteZeroTerms();
        result.sortMonomialsByDegree();
        return result;
    }

    //integrate poly
    public Polynomial integration(Polynomial poly){
        Polynomial result = poly.copyPolynomial();
        for(Monomial monomial: result.getMonomials()){
            int power=monomial.getPower();
            double coefficient=monomial.getCoefficient();
            monomial.setCoefficient(coefficient/(power+1));
            monomial.setPower(power+1);
        }
        result.sortMonomialsByDegree();
        result.deleteZeroTerms();
        return result;
    }
}
