package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class Polynomial {

    private ArrayList<Monomial> monomials = new ArrayList<>();

    public ArrayList<Monomial> getMonomials() {
        return monomials;
    }

    public void sortMonomialsByDegree(){
        Collections.sort(monomials, new Comparator<>() {
            @Override
            public int compare(Monomial o1, Monomial o2) {
                //sorting in descending order of power
                return o2.getPower() - o1.getPower();
            }
        });
    }

    public Polynomial copyPolynomial(){
        Polynomial copyP = new Polynomial();
        for (Monomial monomial : this.getMonomials()) {
            double coefficient = monomial.getCoefficient();
            int power = monomial.getPower();
            copyP.getMonomials().add(new Monomial(coefficient,power));
        }
        return copyP;
    }

    public Monomial searchPower(int degree){
        for (Monomial monomial : this.getMonomials()) {
            if (monomial.getPower() == degree)
                return monomial;
        }
        return null;
    }

    public void addMonomial(Monomial monomial){
        this.getMonomials().add(monomial);
    }

    public boolean equals(Polynomial polynomial){

        ArrayList<Monomial> monomialsP = polynomial.getMonomials();
        ArrayList<Monomial> monomials = this.getMonomials();
        if(monomialsP.size()!=monomials.size())
            return false;
        this.sortMonomialsByDegree();
        polynomial.sortMonomialsByDegree();
        for (int i=0;i<monomials.size();i++){
            if(!monomials.get(i).equals(monomialsP.get(i)))
                return false;
        }
        return true;
    }

    public int degree(){
        this.sortMonomialsByDegree();
        return this.getMonomials().get(0).getPower();
    }

    public void deleteZeroTerms(){
        Iterator itr= this.getMonomials().iterator();
        while(itr.hasNext()){
            Monomial monomial=(Monomial) itr.next();
            if(monomial.getCoefficient()==0){
                itr.remove();
            }
        }
    }
}
