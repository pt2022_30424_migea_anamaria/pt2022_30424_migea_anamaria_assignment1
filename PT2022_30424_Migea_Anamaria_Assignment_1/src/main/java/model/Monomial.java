package model;

public class Monomial {

    private double coefficient;
    private int power;

    public Monomial(double coefficient, int power) {
        this.coefficient = coefficient;
        this.power = power;
    }

    public int getPower() {
        return power;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public boolean equals(Monomial monomial){
        if(monomial.getPower()!=this.getPower())
            return false;
        if(monomial.getCoefficient()!=this.getCoefficient())
            return false;
        return true;
    }
}
